# PER Genie Guide

***
## General Info:
The NUC runs the arduino which handles all communication between Puzzles.
Affected puzzles are:

* the bomb (red puzzle) puzzle in the first room
* the lanterns (yellow puzzle)
* the water puzzle (blue)
* the color tile puzzle (green)
* RFID (gem puzzle)
* Lamp (magnetic switch in pedestal)

Affected Outputs are:

* one relay out from the arduino will trigger the peekaboo controller
	* this triggers the smoke machine and
	* the light above the door
* the second relay out triggers the door itself, causing it to open. this is fired in a short delay after the first

The NUC lives in the ceiling of the second room, roughly above the pedestal. It interfaces with the arduino through a USB interface.

## Troubleshooting Guide:
* verify that the television is turned on
* verify that the NUC is turned on

# Startup Procedure:
 In order to startup the brains

## Configure environment

* install NPM
* install python 3.x
  * pip install flask
  * pip install flask_cors
  * pip install serial
  * pip install phue

## Configure Chrome

```& 'C:\Program Files (x86)\Google\Chrome\Application\chrome.exe' -kiosk http://127.0.0.1:8080/?get_state=127.0.0.1:2900/state --autoplay-policy=no-user-gesture-required```

The two flags ```-kiosk``` and ```--autoplay-policy=no-user-gesture-required```
cause the browser to boot up fullscreen with appropriate policies to allow autoplaying
