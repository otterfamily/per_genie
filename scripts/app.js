
var scene = new THREE.Scene();
var height = window.innerHeight;
var width = window.innerWidth;
//asda
var ANIMATION_MOVEMENT_LIMIT = 30;
var SOLVE_TIME = 45; //in seconds
triggers = [];
var active_puzzle = '';
var intro = document.getElementById("intro");
var race = document.getElementById("race");
var loss = document.getElementById("loss");
var win = document.getElementById("win");
var overlay = document.getElementById("overlay")

var tooltip = document.querySelector('.shadowed');
tooltip.style.display='none';
function playVideo(track){ 
	var vtp;
	if(track == 0){
		vtp = intro;
		console.log("playing intro vid")
	}else if(track ==1){
		vtp = race;
		console.log("playing race vid")
	}else if(track ==2){
		console.log("playing loss vid")
		vtp = loss;
	}else if(track ==3){
		console.log("playing win vid")
		vtp = win;
	}
	if(vtp !== undefined){
		overlay.classList.add("active")
		vtp.classList.remove("disabled")
		vtp.play();
		setTimeout(function(){
			vtp.pause();
			vtp.currentTime = 0;
			vtp.classList.add("disabled")
			overlay.classList.remove("active")
		},vtp.duration*1000);
	}
	
}
// intro.addEventListener("ended",function(){ 
// 	intro.pause();
// 	intro.currentTime = 0;
// 	intro.classList.add("disabled")
// 	overlay.classList.remove("active")
// }); 
// race.addEventListener("ended",function(){
// 	race.pause();
// 	race.currentTime = 0;
// 	race.classList.add("disabled")
// 	overlay.classList.remove("active")
// });
// loss.addEventListener("ended",function(){
// 	loss.pause();
// 	loss.currentTime = 0;
// 	loss.classList.add("disabled")
// 	overlay.classList.remove("active")
// });


var pingServer = function(){
	axios.get('./state')
		.then(function(response){
			cues = response.data.cues;
			triggers= response.data.triggers;
			active_puzzle=response.data.active;
			if(response.data.start_clock == 1){
				tooltip.style.display='initial';
				console.log("STARTED")
				clock.stop();
				clock.start();
			}
			if(response.data.end_clock ==1){	
				console.log("ENDING in 1 second")
				setTimeout(function(){					
					clock.start();
					clock.stop();
					tooltip.style.display='none';
				},1000)
			}
			setTimeout(pingServer,500);
		})
		.catch(function(error){
			console.log(error); 
			setTimeout(pingServer,500);
		})
} 

pingServer();



var renderer = new THREE.WebGLRenderer({antialias:true});
renderer.setSize( width, height );
document.body.appendChild( renderer.domElement );

var white_material = new THREE.MeshBasicMaterial({color:0xffffff})
var grey_material = new THREE.MeshBasicMaterial({color:0xA2A5AA})

var fontLoader = new THREE.FontLoader();

//START LOAD TREE HERE
var ivy_thin, effra_medium, anonymous_pro;
//STYLE INFO
var styles= {};
var unlock_match = false;

var file_loader = new THREE.FileLoader();
var texture_loader = new THREE.TextureLoader();
var object_loader = new THREE.ObjectLoader();

var camera = new THREE.OrthographicCamera( width / - 2, width / 2, height / 2, height / - 2, 1, 1000 );
// var camera = new THREE.PerspectiveCamera();
camera.position.z += 1000;
camera.updateProjectionMatrix();

var title; 
scene.add(camera);

var crystal_top,crystal_bottom,crystal_left,crystal_right;
var clock=new THREE.Clock(false);
var cues = [];
var ring;

object_loader.load("../assets/gem.json",function(gem_obj){
texture_loader.load("../assets/diamond.jpg",function(diamond_tex){
texture_loader.load("../assets/diamond_mask.jpg",function(diamond_mask_tex){
texture_loader.load("../assets/bkg_shrunk.jpg",function(granite_tex){
fontLoader.load("../assets/font/IvyMode Thin_Regular.json",function(_ivy_thin){
	fontLoader.load("../assets/font/Anonymous Pro_Regular.json",function(_anonymous_pro){
			fontLoader.load("../assets/font/Effra App Medium_Regular.json",function(_effra_medium){
			

			var bkg_mat = new THREE.MeshBasicMaterial({color:0xffffff,map:granite_tex})
			var bkg_geo = new THREE.PlaneGeometry(1920,1920,1,1);
			var bkg = new THREE.Mesh(bkg_geo,bkg_mat); 


			scene.add(bkg);
			ivy_thin = _ivy_thin;
			effra_medium = _effra_medium; 


			gem_obj.rotation.x += Math.PI;
			crystal_left = gem_obj.clone(true);
			crystal_left.rotation.z -= Math.PI/2;
			crystal_right = gem_obj.clone(true);
			crystal_right.rotation.z += Math.PI/2;
			crystal_bottom = gem_obj.clone(true);
			crystal_bottom.rotation.z -= Math.PI;
			crystal_top = gem_obj.clone(true);


			var sceneLight = new THREE.HemisphereLight( 0xffffbb, 0x080820, 1 );
			scene.add(sceneLight);

			var offset = 365;
			crystal_left.position.x -= offset;
			crystal_right.position.x += offset;
			crystal_bottom.position.y -= offset;
			crystal_top.position.y += offset;

			crystal_bottom.material = new THREE.MeshPhysicalMaterial();
			crystal_bottom.material.copy(gem_obj.material);
			crystal_right.material = new THREE.MeshPhysicalMaterial();
			crystal_right.material.copy(gem_obj.material);
			crystal_left.material = new THREE.MeshPhysicalMaterial();
			crystal_left.material.copy(gem_obj.material);
			crystal_top.material = new THREE.MeshPhysicalMaterial();
			crystal_top.material.copy(gem_obj.material);

			crystal_left.material.color = new THREE.Color(0x445ee3); //blue
			crystal_right.material.color = new THREE.Color(0xf7f74f); //yellow
			crystal_bottom.material.color = new THREE.Color(0xeb2c1e); //red
			crystal_top.material.color = new THREE.Color(0x44e361); //green


			scene.add(crystal_left);
			scene.add(crystal_right);
			scene.add(crystal_bottom);
			scene.add(crystal_top);

			var ring_geo = new THREE.RingGeometry(215,255,64,2);
			ring_geo = genRing(216,255,64,2,0);
			var ring_mat = new THREE.MeshBasicMaterial({color:0xffffff,opacity:.6,transparent:true,blending:THREE.AdditiveBlending,side:THREE.DoubleSide});
			ring = new THREE.Mesh(ring_geo,ring_mat);
			scene.add(ring);
			styles= {
			margins:{
				small:10,
				base:20,
				large:60
			},
			fonts:{
				primary_total:{
					s:170*.7,
					f:ivy_thin,
					u:false
				},
				secondary_total:{
					s:130*.7,
					f:ivy_thin,
					u:false
				},
				tertiary_content_lg:{
					s:80*.7,
					f:ivy_thin,
					u:false
				},
				tertiary_content_sm:{
					s:60*.7,
					f:ivy_thin,
					u:false
				},
				heading:{
					s:25*.78,
					f:effra_medium,
					u:true
				},
				heading_sm:{
					s:20*.78,
					f:effra_medium,
					u:true
				}

			},
			colors:{
				gala_green:"#0D2D29",
				gala_grey:"#A2A5AA",
				white:"#ffffff"
			}

		};


		animate();

		renderer.setSize( window.innerWidth, window.innerHeight );
		camera.updateProjectionMatrix();
		});
	});
	});	});
});
})});

function genRing(inner,outer,div,divp,completion){
	var r = new THREE.RingGeometry(inner,outer,div,divp,Math.PI/2,-completion*Math.PI*2);
	return r;

}

var dt = 0;
var te = 0;

var ring_completion = 0;

function manage_crystals(completion){
	var oscillation = completion * Math.PI * 35;
	oscillation = Math.cos(oscillation);
	oscillation += 1;
	oscillation *=15;
	oscillation +=5;
	if(!clock.running)oscillation=0;
	var gemscale = 80;
	triggers.forEach(function(t){
		if(t.name == "red"){
			if(t.state){
				crystal_bottom.scale.lerp(new THREE.Vector3(gemscale,gemscale,gemscale),.1);

			}
			else{
				if(active_puzzle == t.name){
				crystal_bottom.scale.lerp(new THREE.Vector3(oscillation,oscillation,oscillation),.1);	
				}
				else{
					crystal_bottom.scale.lerp(new THREE.Vector3(0,0,0),.1)
				}
				
			}
		}
		else if(t.name == "green"){
			if(t.state){
				crystal_top.scale.lerp(new THREE.Vector3(gemscale,gemscale,gemscale),.1);

			}
			else{
				if(active_puzzle == t.name){
				crystal_top.scale.lerp(new THREE.Vector3(oscillation,oscillation,oscillation),.1);	
				}
				else{
					crystal_top.scale.lerp(new THREE.Vector3(0,0,0),.1)
				}
				
			}
		}
		else if(t.name == "blue"){
			if(t.state){
				crystal_left.scale.lerp(new THREE.Vector3(gemscale,gemscale,gemscale),.1);

			}
			else{
				if(active_puzzle == t.name){
				crystal_left.scale.lerp(new THREE.Vector3(oscillation,oscillation,oscillation),.1);	
				}
				else{
					crystal_left.scale.lerp(new THREE.Vector3(0,0,0),.1)
				}
				
			}
		}
		else if(t.name == 'gold'){
			if(t.state){
				crystal_right.scale.lerp(new THREE.Vector3(gemscale,gemscale,gemscale),.1);

			}
			else{
				if(active_puzzle == t.name){
					crystal_right.scale.lerp(new THREE.Vector3(oscillation,oscillation,oscillation),.1);	
				}
				else{
					crystal_right.scale.lerp(new THREE.Vector3(0,0,0),.1)
				}
				
			}

		}
	});

}


function resolve_cues(){
	while(cues.length >0){
		cue = cues.pop();
		console.log("resolving "+cue.name);
		resolve_cue(cue);
	}
}

function resolve_cue(c){

	if(c.type == 'audio'){
	}else if(c.type == 'video'){
		playVideo(c.cue_number);
	}
}
var animate = function () {
	var c = 0;
	c =clock.getElapsedTime();
	c /= SOLVE_TIME;
	if(c >= 1){
		c = 1;
		setTimeout(function(){clock.start();clock.stop()},1500);
	}
	manage_crystals(c); 
	
	
	resolve_cues();
	
	runRing(c);
	requestAnimationFrame( animate );
	renderer.render(scene,camera);	
	
};

function runRing(c){
	ring_completion = lerp(ring_completion,c,.1);
	if(ring_completion>.0001){
		ring.visible = true;
		ring.geometry = genRing(216,255,64,2,ring_completion)	
	}else{
		ring.visible = false;
	}
}



function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}


function lerp(a,b,x){
	return a+(b-a)*x;
}
var reset= function(){
	activeNames = [];
	filling = false;
	// var el = document.getElementById("progressSlider");
	// el.value = 0;
	STATE=FILL;
	targetProgress = 0;
	currentProgress=0;
	scene = new THREE.Scene();
	 
}

var formatNumber= function(x){
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}


