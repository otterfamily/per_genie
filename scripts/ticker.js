/*
	example data:{
		position:new THREE.Vector3(0,0,0),
		textSize:64,
		font:sans_font,
		prefix: "$",
		suffix: " raised",
		initialText: 0
	}
*/

var Ticker = function(data){
	this.setup= function(data){
		this.myGroup = new THREE.Group();
		this.dynamic= data.dynamic;
		this.position = data.position;
		this.textSize = data.textSize;
		this.font = data.font;
		this.prefix = data.prefix;
		this.suffix = data.suffix;
		this.labelText = data.initialText;
		this.myMaterial = data.material;
		this.halignment = data.halignment;
		this.valignment = data.valignment;
		this.parent = (!!data.parent)?data.parent:scene;
		this.myText = this.generate(data.prefix+ data.initialText+data.suffix,this.textSize,this.font);
	}


	this.run=function(newText){
		newText = formatNumber(newText);
		if(newText != this.labelText && this.dynamic == true){
			this.labelText = newText;
			newText = this.prefix+newText;
			newText = newText+this.suffix;
			this.myText = this.generate(newText,this.textSize,this.font);
		}else if(this.position.distanceTo(this.myMesh.position) > 1){
			this.myMesh.position.copy(this.position);
			this.myMesh.geometry.computeBoundingBox();
			var bb = this.myMesh.geometry.boundingBox;
			if(this.halignment == 'c')this.myMesh.position.x -= bb.max.x /2;
			else if(this.halignment == 'r')this.myMesh.position.x -= bb.max.x;
			if(this.valignment == 'c')this.myMesh.position.y -= bb.max.y/2;
			else if(this.valignment == 't') this.myMesh.position.y -= bb.max.y;

		}
	}

	this.generate = function(txt,size,font){
		if(this.myMesh instanceof THREE.Mesh){
			this.parent.remove(this.myMesh); //first ensure that it is completely gone. dynamic text is a bad culprit for clogging memory
			this.myMesh.geometry.dispose();
			this.myMesh = null;
			delete this.myMesh;
		}
		var shapes=  font.generateShapes(txt,size);
		var geo = new THREE.ShapeBufferGeometry(shapes);
		this.myMesh = new THREE.Mesh(geo,this.myMaterial);
		this.myMesh.geometry.computeBoundingBox();
		var bb = this.myMesh.geometry.boundingBox;
		this.parent.add(this.myMesh);
		this.myMesh.position.add(this.position);
		if(this.halignment == 'c')this.myMesh.position.x -= bb.max.x /2;
		else if(this.halignment == 'r')this.myMesh.position.x -= bb.max.x;
		if(this.valignment == 'c')this.myMesh.position.y -= bb.max.y/2;
		else if(this.valignment == 't') this.myMesh.position.y -= bb.max.y;
	} 



	this.setup(data);
}