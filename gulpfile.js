var gulp = require('gulp');
var gutil = require('gulp-util'),
	uglify = require('gulp-uglify'),
	concat = require('gulp-concat'),
	connect= require('gulp-connect');

var jsSources = ['./scripts/vendor/*.js','./scripts/**'],
    cssSources = ['styles/*.css'],
    htmlSources = ['index.html','./styles/*.css','assets/**'],
    outputDir = 'dist',
    assetsDir = "./assets",
    scriptOutputDir = 'dist/scripts';

gulp.task('log',function(){
	gutil.log('==log debug==')
});

gulp.task('js',function(){
	gulp.src(jsSources)
	.pipe(concat('scripts.js'))
	.pipe(gulp.dest(scriptOutputDir))
	.pipe(connect.reload())
});




gulp.task('copy',function(){
	gulp.src('index.html')
	.pipe(gulp.dest(outputDir));
	gulp.src('assets/**')
	.pipe(gulp.dest("dist/assets"));
	gulp.src(cssSources)
	.pipe(gulp.dest("dist/styles"));
});


gulp.task('watch',function(){
	gulp.watch(jsSources,['js'])
	gulp.watch(htmlSources,['copy'])
});


gulp.task('connect',function(){
	connect.server({
		root:'./dist',
		livereload:true
	})
});


gulp.task('html',function(){
	gulp.src(htmlSources)
	.pipe(connect.reload())
}); 



gulp.task('default',['html', 'js','connect','watch']) 