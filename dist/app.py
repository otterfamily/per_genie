from flask import Flask, jsonify, request, send_from_directory, redirect, render_template
from flask_cors import CORS
import sys
import random
import math
import serial
from serial.tools import list_ports
from serial import SerialException
import threading
import copy
import signal
from threading import Timer
from playsound import playsound

from phue import Bridge
from time import sleep
import pygame


pygame.mixer.init()
pygame.mixer.music.load("race.mp3")


bridge= False

ambient_brightness = 192
try:
    bridge = Bridge(sys.argv[3])
    # If the app is not registered and the button is not pressed, press the button and call connect() (this only needs to be run a single time)
    bridge.connect()

    # Get the bridge state (This returns the full dictionary that you can explore)
    bridge.get_api()
except:
    print("hue was unreachable")
#hue range is up to 65535



print("Available com ports are: ")
for com in list_ports.comports():
    print(com)
port = sys.argv[1]
ser = False
try:
    serialport = sys.argv[2]
    ser = serial.Serial(serialport,9600)
    print("connected to port {}".format(serialport))
except:
    print('serial port is unable to open')
    ser = False


current_state = ""
app = Flask(__name__, static_url_path='', static_folder="")
app.config.from_object(__name__)
CORS(app) 
phase = 0
last_red_reading = False
last_green_reading = False
puzzle_state = { 
    'phase':1,
    'last_red_reading':False,
    'last_green_reading':False,
    'triggers':[
        {   "name":'gold',
            "pin":12,
            "state":False
        },
        {   "name":'red',
            "pin":11,
            "state":False
        },
        {   "name":'blue',
            "pin":10,
            "state":False
        },
        {   "name":'green',
            "pin":9,
            "state":False
        },
        {   "name":'rfid',
            "pin":8,
            "state":False
        },
        { "name":'lamp',
            "pin":7,
            "state":False
        },
        { "name":'race',
            "pin":6,
            "state":False
        }
        ],
    'cues':[],
    'active':'',
    'start_clock':'0',
    'end_clock':'0'
}

p_puzzle_state = copy.deepcopy(puzzle_state)

def get_state():
    s = jsonify(puzzle_state)
    return s

@app.route('/puzzle/<index>/<state>')
def set_puzzle(index,state):
    if index == 'clock':
        if state == '1':
            puzzle_state['start_clock'] = '1'
        else:
            puzzle_state['end_clock'] = '1'
    else:
        print("setting puzzle {} to {}".format(index,state))
        puzzle_state['triggers'][int(index)]["state"] = True if (state == '1') else False
        check_state()
    return redirect('/controls');


    
@app.route('/controls') 
def controls():
    return app.send_static_file("./controls.html")

@app.route('/state')
def state():
    s = get_state()
    if(puzzle_state['start_clock'] == '1'):
        puzzle_state['start_clock'] = '0'
    if(puzzle_state['end_clock'] == '1'):
        puzzle_state['end_clock'] = '0'
    if(len(puzzle_state['cues']) > 0):
        #clear out any cues so that we don't get repeats
        puzzle_state['cues'] = []
    return s

@app.route("/open_door")
def open_door_route():   
    open_door_thread = threading.Thread(target=open_door_routine)    
    open_door_thread.daemon = True
    open_door_thread.start()
    return redirect('/controls')


@app.route("/close_door")
def close_door_route():
    close_door_thread = threading.Thread(target=close_door_routine)
    close_door_thread.daemon = True
    close_door_thread.start()
    return redirect('/controls')

@app.route('/light_show')
def light_show_route():    
    light_show_thread = threading.Thread(target=light_show)    
    light_show_thread.daemon = True
    light_show_thread.start()
    return redirect('/controls')

@app.route('/playvid/<index>')
def add_video_cue(index):
    video_cue = {
            'name':'moderator_command',
            'type':'video',
            'cue_number':int(index)
    }     
    puzzle_state['cues'].append(video_cue)
    if index == '3' or index =='2':        
        pygame.mixer.music.stop()
    return redirect('/controls')


@app.route('/initialize')
def initialize():
    hard_reset()
    return redirect('/controls')

@app.route('/')
def display():
    return app.send_static_file("./index.html")

def open_door_routine():
    # sleep(30)
    print("opening door")
    if ser:
        ser.write(b'1')
        sleep(1)
    else:
        print("ERROR, NO COMMUNICATION WITH SERIAL")

def close_door_routine():
    print("closing the door")
    if ser:
        ser.write(b'0')
        sleep(1)
    else:
        print("ERROR, NO COMMUNICATION WITH SERIAL")
def smoke_routine():
    sleep(55)
    print("DOING SMOKE ROUTINE")
    if ser:
        ser.write(b'3')
        sleep(3)
        ser.write(b'2')
    else:
        print("ERROR, NO COMMUNICATION WITH SERIAL")


    print("waiting to open door")
    sleep(2) #triggers after full 62 seconds
    open_door_routine()

def restart_red():
    #special routine to address the power cycling need of the red puzzle
    if bridge:        
        bridge.set_light("Red puzzle", 'on', False)
        sleep(.5)
        bridge.set_light("Red puzzle",'on',True)

def restart_green():
    #special routine to address the power cycling need of the red puzzle
    if bridge:        
        bridge.set_light("Green puzzle", 'on', False)
        sleep(.5)
        bridge.set_light("Green puzzle",'on',True)



def light_show():
    h = 0
    s = 255
    b = 126
    light_names = ["Green light","Red light","Blue light","Yellow light"]
    if bridge:  
        for x in range(75):
            h += 120
            b = random.randint(0,255)
            h %= 255
            set_light(light_names[x%4],random.randint(0,255),s,b)
    handle_lighting()


def solved_phase_1():
    #go to phase 2
    puzzle_state['phase'] = 2
    
    smoke_machine_thread = threading.Thread(target=smoke_routine)    
    smoke_machine_thread.daemon = True
    smoke_machine_thread.start()

    light_show_thread = threading.Thread(target=light_show)    
    light_show_thread.daemon = True
    light_show_thread.start()

    video_cue = {
            'name':'opening door video',
            'type':'video',
            'cue_number':0
    }     
    puzzle_state['cues'].append(video_cue) 

def solved_phase_2():
    triggers = puzzle_state['triggers']
    #reset all triggers
    for trigger in triggers:
        trigger['state'] = False
    #go to phase 3
    video_cue = {
            'name':'starting race condition video',
            'type':'video',
            'cue_number':1
    }
    close_door_thread = threading.Thread(target=close_door_routine)
    close_door_thread.daemon = True
    close_door_thread.start()

    puzzle_state['cues'].append(video_cue)

    light_show_thread = threading.Thread(target=light_show)    
    light_show_thread.daemon = True
    light_show_thread.start()

    puzzle_state['phase'] = 3
    sleep(55) #this is the length of the video
    run_race(False)
    set_all_active()

def solved_phase_3():

    triggers = puzzle_state['triggers']
    puzzle_state['phase'] = 4
    puzzle_state['end_clock'] = '1'

def solved_phase_4():

    print("reverting back to phase one, ready to play again")
    triggers = puzzle_state['triggers']
    for trigger in triggers:
        trigger['state'] = False
    puzzle_state['phase'] = 1
    puzzle_state['end_clock'] = '1'

def hard_reset():
    pygame.mixer.music.stop()
    print("receiving hard reset from remote - resetting puzzle to presentation ready")
    set_lights_ambient()
    puzzle_state['phase'] = 1
    puzzle_state['end_clock'] = '1'
    close_door_routine()

    triggers = puzzle_state['triggers']
    for trigger in triggers:
        trigger['state'] = False
def set_all_active():
    set_light("Yellow light",255/8,255,255)
    set_light("Red light",0,255,255)
    set_light("Blue light",140,255,255)
    set_light("Green light",100,255,255)

def handle_lighting():    
    triggers = puzzle_state['triggers']
    if triggers[0]['state']:
        set_light("Yellow light",255/8,255,255)
    else:
        set_light("Yellow light",255/7,ambient_brightness,255)

    if triggers[1]['state']:
        set_light("Red light",0,255,255)
    else:
        set_light("Red light",255/7,ambient_brightness,255)

    if triggers[2]['state']:
        set_light("Blue light",140,255,255)
    else:
        set_light("Blue light",255/7,ambient_brightness,255)
    
    if triggers[3]['state']:
        set_light("Green light",100,255,255)
    else:
        set_light("Green light",255/7,ambient_brightness,255)

def set_light(index, hue, brightness, saturation):
    hue %= 255
    hue /= 255
    hue *= 65535
    hue = int(hue)
    try:
        bridge.set_light(index, 'hue', hue)
        bridge.set_light(index,'bri',brightness)
        bridge.set_light(index,'sat',saturation)
    except:
        print("error communicating with the lights")
def set_lights_ambient():
    set_light("Yellow light",255/7,ambient_brightness,255)
    set_light("Red light",255/7,ambient_brightness,255)
    set_light("Blue light",255/7,ambient_brightness,255)
    set_light("Green light",255/7,ambient_brightness,255)  
    if bridge:  
        bridge.set_light("Red puzzle",'on',True)

set_lights_ambient()


# LIGHT REFERENCE
# TEAL/BLUE set_light(#,126,255,255)
# RED set_light(#,0,255,255)
# GOLD set_light(#,255/6,255,255)
# GREEN set_light(#,2*255/6,255,255)

def run_race(repeat):
    if(puzzle_state['phase'] == 3):
        if(repeat):
            playsound('tryagain.mp3')
        pygame.mixer.music.stop()
        pygame.mixer.music.play()

        print("starting race")
        triggers = puzzle_state['triggers']
        puzzle_state['active'] = triggers[0]['name']
        for trigger in triggers:
            trigger['state'] = False
        puzzle_state['start_clock'] = '1'
        race_timer = Timer(47.0,run_race,[True])
        race_timer.start()
    else:
        print("timer timed out, but phase has changed")
    #start a timer
    #send a signal to start timer
    #when timer is up, if we are still in phase 4, resend signal to start timer

def check_state():
    phase = puzzle_state['phase']
    triggers = puzzle_state['triggers']
    for trigger in triggers:
        print(trigger)
    active = puzzle_state['active']
    if phase == 1:
        #check for case of phase 1 solved
        print("currently in phase one")
        solved = (triggers[5]['state'])
        if solved:
            print("phase 1 solved!")
            solved_phase_1()
    elif phase == 2:
        print("currently in phase two")
        #check for case of phase 2 solved
        solved = (triggers[4]['state'])
        if solved:
            solved_phase_2()
    elif phase == 3:
        print("currently in phase four")
        solve = True
        active = ''
        for x in range(4):
            trigger = triggers[x]
            if not trigger['state']:
                solve = False
                if active == '':
                    active = trigger['name']
            trigger['state'] = solve
        puzzle_state['active'] = active
        all_complete = True
        for x in range(4):
            trigger = triggers[x]
            #if puzzle is not solved, then all are not solved
            if not trigger['state']:
                all_complete = False
        if all_complete:
            print("SOLVED PHASE 4")
            solved_phase_3()
    elif phase == 4:
        print("currently in phase 5")
        reset = True
        for trigger in triggers:
            if trigger['state'] == True:
                print(trigger)
                reset = False
        if reset:
            print("no puzzles reporting high, solved phase 5")
            solved_phase_4()
        else:
            print("a puzzle is still reporting high")


    handle_lighting()


            
def handle_data(data):

    if data[:2]=="XX":
        data = data[2:]
        trigger_readings = []
        data = data.split('\r')[0]
        data = data.split(',')
        for entry in data:
            entry = entry.split(':')    

            trigger_readings.append(entry)
        triggers = puzzle_state['triggers']
        for trigger in triggers:
            matched = False
            if trigger['state'] == False:
                for trigger_reading in trigger_readings:
                    if int(trigger_reading[0]) == trigger['pin']:
                        if trigger_reading[1] == "1":
                            trigger['state'] = True
            elif puzzle_state['phase'] == 5:
                for trigger_reading in trigger_readings:
                    if int(trigger_reading[0]) == trigger['pin']:
                        if trigger_reading[1] == "0":
                            trigger['state'] = False
                        else:
                            trigger['state'] = True
            #handle the issue with red - we need an internal model of every time it flops even if it doesnt match our representation
            if trigger['pin'] == 11:
                for trigger_reading in trigger_readings:
                    if int(trigger_reading[0]) == 11:
                        if puzzle_state['last_red_reading'] == False and trigger_reading[1] == "1":
                            restart_red()
                        puzzle_state['last_red_reading'] = True if (trigger_reading[1] == '1') else False
            if trigger['pin'] == 9:
                for trigger_reading in trigger_readings:
                    if int(trigger_reading[0]) == 9:
                        if puzzle_state['last_green_reading'] == False and trigger_reading[1] == "1":
                            restart_green()
                        puzzle_state['last_green_reading'] = True if (trigger_reading[1] == '1') else False
        check_state()
    else:
        print(data)
def read_from_port():
    while True:
        if ser:
            data = ser.readline().decode()
            handle_data(data)
        else:
            print("serial is busted")
            sleep(1)
thread = threading.Thread(target=read_from_port)
thread.daemon = True

def keyboardInterruptHandler(signal, frame):
    print("KeyboardInterrupt (ID: {}) has been caught. Cleaning up...".format(signal))
    sys.exit(0)

signal.signal(signal.SIGINT, keyboardInterruptHandler)

if __name__ == '__main__':
    thread.start()
    app.run(host='0.0.0.0', port=port)

