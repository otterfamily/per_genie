uniform float progress;
uniform sampler2D texture;
uniform vec2 resolution;
uniform vec2 resolution_window;
uniform vec3 noise_center;
uniform vec2 resolution_offset;
uniform vec2 grid;
uniform vec2 grid_offset;
varying vec2 vUv;
uniform float flicker;
float mod289(float x){return x - floor(x * (1.0 / 289.0)) * 289.0;}
vec4 mod289(vec4 x){return x - floor(x * (1.0 / 289.0)) * 289.0;}
vec4 perm(vec4 x){return mod289(((x * 34.0) + 1.0) * x);}

float noise(vec3 p){
    p += noise_center;
    vec3 a = floor(p);
    vec3 d = p - a;
    d = d * d * (3.0 - 2.0 * d);

    vec4 b = a.xxyy + vec4(0.0, 1.0, 0.0, 1.0);
    vec4 k1 = perm(b.xyxy);
    vec4 k2 = perm(k1.xyxy + b.zzww);

    vec4 c = k2 + a.zzzz;
    vec4 k3 = perm(c);
    vec4 k4 = perm(c + 1.0);

    vec4 o1 = fract(k3 * (1.0 / 41.0));
    vec4 o2 = fract(k4 * (1.0 / 41.0));

    vec4 o3 = o2 * d.z + o1 * (1.0 - d.z);
    vec2 o4 = o3.yw * d.x + o3.xz * (1.0 - d.x);

    return o4.y * d.y + o4.x * (1.0 - d.y);
}


void main()
{  
    
    vec2 fragCoord = vUv*resolution_window; //vUv is now pixel space
    fragCoord += resolution_offset;

    fragCoord /= resolution;

    vec2 aspect = vec2(1.0,resolution.y/resolution.x);
    vec2 co = floor(fragCoord * grid);
    co.y = grid.y-co.y;
    float my_index = co.y + co.x*grid.y;
    
    
    vec4 tex_samp = texture2D(texture,fragCoord);
    // tex_samp = vec4(smoothstep(0.0,0.9,tex_samp.a));
    
    float margin = .25;
    float a = smoothstep(my_index,my_index+margin,progress);
    float flash = 1.0-smoothstep(0.0,margin/2.0,abs(progress-my_index));
    
    float n = noise(vec3(fragCoord*aspect*120.0+vec2(0.0,0.0),1.0));
    n = smoothstep(0.1,0.5,n);
    float pn = n;
    n *= 0.4;
    vec4 a_color =tex_samp ;
    vec4 b_color = mix(tex_samp*n,a_color,.3);

    vec4 col = mix(a_color,b_color,1.0-a);

    vec4 flash_color = vec4(step(0.2,col.a));
    col = mix(col,flash_color,flash);

    col = mix(col,mix(tex_samp*pn,a_color,.3),flicker);
    // Output to screen
    gl_FragColor = col;
}