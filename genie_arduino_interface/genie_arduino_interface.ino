String version = "genie_interface_v0.1 VV"; //version and startup delimiter at VV
bool puzzle_gold = false,
     puzzle_red = false,
     puzzle_blue = false,
     puzzle_green = false,
     puzzle_race = false,
     puzzle_rfid = false,
     puzzle_lamp = false;
int PEDESTAL_OUT_PIN = 2,
    SMOKE_OUT_PIN = 3,
    AUX_OUT_PIN = 4,
    PUZZLE_GOLD_PIN = 12,
    PUZZLE_RED_PIN = 11,
    PUZZLE_BLUE_PIN = 10,
    PUZZLE_GREEN_PIN = 9,
    PUZZLE_RFID_PIN = 8,
    LAMP_PIN = 7,
    RACE_PIN = 6;
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  while (!Serial) {
    ;
  }
  //print out version and pin information

  pinMode(PEDESTAL_OUT_PIN, OUTPUT);
  Serial.println("PEDESTAL RELAY TRIGGER ON PIN: " + String(PEDESTAL_OUT_PIN));

  pinMode(SMOKE_OUT_PIN, OUTPUT);
  Serial.println("SMOKE RELAY TRIGGER ON PIN: " + String(SMOKE_OUT_PIN));

  pinMode(AUX_OUT_PIN, OUTPUT);
  Serial.println("AUXILLARY RELAY TRIGGER ON PIN: " + String(AUX_OUT_PIN));

  pinMode(PUZZLE_GOLD_PIN, INPUT);
  Serial.println("PUZZLE_GOLD_PIN: " + String(PUZZLE_GOLD_PIN));

  pinMode(PUZZLE_RED_PIN, INPUT);
  Serial.println("PUZZLE_RED_PIN: " + String(PUZZLE_RED_PIN));

  pinMode(PUZZLE_BLUE_PIN, INPUT);
  Serial.println("PUZZLE_BLUE_PIN: " + String(PUZZLE_BLUE_PIN));

  pinMode(PUZZLE_GREEN_PIN, INPUT);
  Serial.println("PUZZLE_GREEN_PIN: " + String(PUZZLE_GREEN_PIN));

  pinMode(PUZZLE_RFID_PIN, INPUT);
  Serial.println("PUZZLE_RFID_PIN: " + String(PUZZLE_RFID_PIN));

  pinMode(LAMP_PIN, INPUT);
  Serial.println("LAMP_PIN: " + String(LAMP_PIN));

  
  pinMode(RACE_PIN, INPUT);
  Serial.println("RACE_PIN: " + String(RACE_PIN));

  Serial.println(version);

}

void loop() {
  //INIT PUZZLE READING VALUES
  bool puzzle_gold_reading = false,
       puzzle_red_reading = false,
       puzzle_blue_reading = false,
       puzzle_green_reading = false,
       puzzle_rfid_reading = false,
       puzzle_race_reading = false,
       puzzle_lamp_reading = false;
  //GET UP TO DATE PUZZLE READINGS
  puzzle_gold_reading = digitalRead(PUZZLE_GOLD_PIN);
  puzzle_red_reading = digitalRead(PUZZLE_RED_PIN);
  puzzle_blue_reading = digitalRead(PUZZLE_BLUE_PIN);
  puzzle_green_reading = digitalRead(PUZZLE_GREEN_PIN);
  puzzle_rfid_reading = digitalRead(PUZZLE_RFID_PIN);
  puzzle_lamp_reading = digitalRead(LAMP_PIN);
  puzzle_race_reading = digitalRead(RACE_PIN);
  bool send_update = false;
  //IF ANYTHING HAS CHANGED, SEND THOSE UPDATES OVER SERIAL
  if (puzzle_gold_reading != puzzle_gold ||
      puzzle_red_reading != puzzle_red ||
      puzzle_blue_reading != puzzle_blue ||
      puzzle_green_reading != puzzle_green ||
      puzzle_rfid_reading != puzzle_rfid ||
      puzzle_race_reading != puzzle_race ||
      puzzle_lamp_reading != puzzle_lamp) {

    send_update = true;
  }

  //UPDATE TO NEW VALUES
  puzzle_gold = puzzle_gold_reading;
  puzzle_red = puzzle_red_reading;
  puzzle_blue = puzzle_blue_reading;
  puzzle_green = puzzle_green_reading;
  puzzle_rfid = puzzle_rfid_reading;
  puzzle_lamp = puzzle_lamp_reading;
  puzzle_race = puzzle_race_reading;


  if (send_update)send_updates();

}


void send_updates() {
  String packet = "XX";
  packet += keyValue(PUZZLE_GOLD_PIN, puzzle_gold);
  packet += ",";
  packet += keyValue(PUZZLE_RED_PIN, puzzle_red);
  packet += ",";
  packet += keyValue(PUZZLE_BLUE_PIN, puzzle_blue);
  packet += ",";
  packet += keyValue(PUZZLE_GREEN_PIN, puzzle_green);
  packet += ",";
  packet += keyValue(PUZZLE_RFID_PIN, puzzle_rfid);
  packet += ",";
  packet += keyValue(LAMP_PIN, puzzle_lamp);
  packet += ",";
  packet += keyValue(RACE_PIN, puzzle_race);


  Serial.println(packet);
}

String keyValue(int pin, bool state) {
  String kv = String(pin);
  kv += ":";
  kv += String(state);
  return kv;
}

void serialEvent() {
  String inputString = "";
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();
    inputString += inChar;
    if (inChar == '0') {
      digitalWrite(PEDESTAL_OUT_PIN, LOW);
    } else if (inChar == '1') {
      digitalWrite(PEDESTAL_OUT_PIN, HIGH);
    } else if (inChar == '2') {
      digitalWrite(SMOKE_OUT_PIN, LOW);
    } else if (inChar == '3') {
      digitalWrite(SMOKE_OUT_PIN, HIGH);
    }else if (inChar == '4') {
      digitalWrite(AUX_OUT_PIN, LOW);
    }else if (inChar == '5') {
      digitalWrite(AUX_OUT_PIN, HIGH);
    }
    // add it to the inputString:
    // if the incoming character is a newline, set a flag so the main loop can
    // do something about it:
  }
}
